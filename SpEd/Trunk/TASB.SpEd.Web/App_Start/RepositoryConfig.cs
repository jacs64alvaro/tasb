﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TASB.SpEd.Business;

namespace TASB.SpEd.Web
{
    partial class Startup
    {
        public void ConfigureRepositories(IAppBuilder app)
        {
            app.CreatePerOwinContext<ApplicationRepository>(ApplicationRepository.Create);
            app.CreatePerOwinContext<StudentRepository>(StudentRepository.Create);
            app.CreatePerOwinContext<LocationRepository>(LocationRepository.Create);
        }
    }
}