﻿using nJupiter.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TASB.SpEd.Util.Configuration
{
    public static class ConfigManager
    {
        public static XmlNode GetNode(string xpath)
        {
            return ConfigRepository.Instance.Configurations
                .Select(c => c.ConfigXml.SelectSingleNode(xpath))
                .Where(n => n != null)
                .FirstOrDefault();
        }

        public static IConfig GetConfigFile(string name)
        {
            return ConfigRepository.Instance.Configurations
                .Where(e => e.ConfigKey == name)
                .FirstOrDefault();
        }
    }
}
