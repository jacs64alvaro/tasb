﻿using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TASB.SpEd.DataAccess;
using TASB.SpEd.DataAccess.Models;

namespace TASB.SpEd.Business
{
    public class StudentRepository : BaseRepository
    {
        public static StudentRepository Create()
        {
            var repository = new StudentRepository();
            repository.Context = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            return repository;
        }

        public async Task RegisterStudentAsync(Student student)
        {
            Context.Students.Add(student);
            await Context.SaveChangesAsync();
        }

        public List<Student> GetStudentsByLocation(string id)
        {
            return Context.Students
                .Where(a => a.LocationId == id).ToList();
        }

        public List<Student> GetAllStudents()
        {
            return Context.Students
                .Include(s => s.Location)
                .ToList();
        }

        public List<Student> GetStudentsByFirstName(string name)
        {
            return Context.Students
                .SqlQuery("GetStudentsByFirstName @name",new SqlParameter("name",name))
                .ToList();
        }

        public List<Student> GetStudentsByLocationName(string location)
        {
            return Context.Students
                .SqlQuery("GetStudentsByLocation @location", new SqlParameter("location", location))
                .ToList();
        }
    }
}
