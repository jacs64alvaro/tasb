﻿using System;
using System.Collections.Generic;

namespace TASB.SpEd.DataAccess.Models
{
    public class TasbRole
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TasbRole> TasbRoles { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }

        public TasbRole()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
