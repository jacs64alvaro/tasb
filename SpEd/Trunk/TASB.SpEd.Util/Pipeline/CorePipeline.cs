﻿using System;
using System.Threading.Tasks;
using System.Xml;

namespace TASB.SpEd.Util.Pipeline
{
    public static class CorePipeline
    {
        public static void Run(string pipeline, PipelineArgs args)
        {
            var processors = PipelineReader.GetPipelineProcessors(pipeline);
            if (processors == null)
            {
                return;
            }
            foreach (XmlNode processor in processors)
            {
                if (args.Abort)
                    break;

                var type = processor.Attributes["type"].Value;
                var method = processor.Attributes["method"].Value;
                args.Parameters = PipelineReader.GetParameters(processor);

                ExecuteProcessor(type, method, args);
            }
        }

        public static async Task RunAsync(string pipeline, PipelineArgs args)
        {
            await Task.Factory.StartNew(() => Run(pipeline, args));
        }

        private static void ExecuteProcessor(string type, string method, PipelineArgs args)
        {
            var loadedType = Type.GetType(type);

            if (loadedType == null)
            {
                throw new TypeLoadException(string.Format("Type not found: {0}", type));
            }

            var methodInfo = loadedType.GetMethod(method, new Type[] { typeof(PipelineArgs) });
            if (methodInfo == null)
            {
                throw new TypeLoadException(string.Format("Method not found: {0}", method));
            }

            var classInstance = Activator.CreateInstance(loadedType, null);

            if (classInstance == null)
            {
                throw new TypeLoadException(string.Format("Could not create instance of type: {0}", type));
            }

            methodInfo.Invoke(classInstance, new object[] { args });
        }
    }
}
