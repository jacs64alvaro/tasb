﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TASB.SpEd.DataAccess;
using TASB.SpEd.DataAccess.Models;
using System.Web;
using Microsoft.AspNet.Identity.Owin;

namespace TASB.SpEd.Business
{
    public class LocationRepository : BaseRepository
    {
        public static LocationRepository Create()
        {
            var repository = new LocationRepository();
            repository.Context = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            return repository;
        }

        public async Task RegisterLocationAsync(Location location)
        {
            Context.Locations.Add(location);
            await Context.SaveChangesAsync();
        }

        public Location GetLocationById(string id)
        {
            return Context.Locations
                .First(a => a.Id == id);
        }
    }
}
