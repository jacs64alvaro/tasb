﻿using System;

namespace TASB.SpEd.DataAccess.Models
{
    public class SecurityAnswer
    {
        public string Id { get; set; }

        public string QuestionId { get; set; }

        public string Answer { get; set; }

        public SecurityQuestion Question { get; set; }

        public SecurityAnswer()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
