﻿using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using TASB.SpEd.Util.Configuration;

namespace TASB.SpEd.Util.Pipeline
{
    public class PipelineReader
    {

        private static XmlNode GetPipeline(string name)
        {
            return ConfigManager.GetNode(string.Format("pipelines/pipeline[@name='{0}']", name));
        }

        internal static XmlNodeList GetPipelineProcessors(string name)
        {
            var pipeline = GetPipeline(name);
            if (pipeline == null)
            {
                return null;
            }
            return pipeline.ChildNodes;
        }

        internal static Dictionary<string, string> GetParameters(XmlNode processor)
        {
            var parameters = new Dictionary<string, string>();
            foreach (XmlNode parameter in processor.ChildNodes)
            {
                parameters.Add(parameter.Attributes["name"].Value, parameter.Attributes["value"].Value);
            }

            return parameters;
        }

    }
}
