﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TASB.SpEd.DataAccess.Models;

namespace TASB.SpEd.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<SecurityQuestion> SecurityQuestions { get; set; }
        public DbSet<TasbRole> TasbRoles { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Location> Locations { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(u => u.TasbRoles)
                .WithMany(r => r.Users)
                .MapToStoredProcedures()
                .Map(c => c.ToTable("TasbRolesPerUser"));

            modelBuilder
                .Entity<SecurityQuestion>()
                .MapToStoredProcedures();

            modelBuilder
                .Entity<Student>()
                .MapToStoredProcedures();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
