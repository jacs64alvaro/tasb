﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TASB.SpEd.Web.Models
{
    public class StudentViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Location { get; set; }
    }

    public class ListStudentsViewModel
    {
        public List<StudentViewModel> Students { get; set; }
    }
}