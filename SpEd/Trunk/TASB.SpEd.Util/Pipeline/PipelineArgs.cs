﻿using System.Collections.Generic;

namespace TASB.SpEd.Util.Pipeline
{
    public class PipelineArgs
    {
        private Dictionary<string, object> _data;

        public bool Abort { get; set; }
        public Dictionary<string, object> Data
        {
            get
            {
                if (_data == null)
                {
                    _data = new Dictionary<string, object>();
                }
                return _data;
            }
        }
        public Dictionary<string, string> Parameters { get; set; }
    }
}
