﻿using System.Web;
using System.Web.Mvc;

namespace TASB.SpEd.Business.Authorization
{
    public class HasPermissionAttribute : AuthorizeAttribute
    {
        private string _permission;

        public HasPermissionAttribute(string permission)
        {
            this._permission = permission;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var result = base.AuthorizeCore(httpContext);
            if (result)
            {
                return httpContext.User.Identity.HasPermission(httpContext, _permission);
            }
            return false;
        }

    }
}