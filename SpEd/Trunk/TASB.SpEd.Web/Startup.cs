﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TASB.SpEd.Web.Startup))]
namespace TASB.SpEd.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureRepositories(app);
        }
    }
}
