﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TASB.SpEd.DataAccess;

namespace TASB.SpEd.Business
{
    public class BaseRepository : IDisposable
    {
        private ApplicationDbContext _context;

        public ApplicationDbContext Context
        {
            get
            {
                return _context;
            }
            protected set
            {
                _context = value;
            }
        }
        
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
