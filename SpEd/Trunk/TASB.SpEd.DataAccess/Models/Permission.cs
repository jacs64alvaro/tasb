﻿using System;
using System.Collections.Generic;

namespace TASB.SpEd.DataAccess.Models
{
    public class Permission : IEquatable<Permission>
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public List<TasbRole> Roles { get; set; }

        public Permission()
        {
            Id = Guid.NewGuid().ToString();
        }

        bool IEquatable<Permission>.Equals(Permission other)
        {
            return other.Id == this.Id;
        }
    }
}
