﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using TASB.SpEd.DataAccess.Models;
using TASB.SpEd.DataAccess;
using TASB.SpEd.Util;
using System.Web;
using Microsoft.AspNet.Identity.Owin;

namespace TASB.SpEd.Business
{
    public class ApplicationRepository : BaseRepository
    {
        public static ApplicationRepository Create()
        {
            var repository = new ApplicationRepository();
            repository.Context = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            return repository;
        }

        public SecurityAnswer GetSecurityAnswerByUser(ApplicationUser user, string questionId)
        {
            return user.SecurityAnswers
                .Where(a => a.QuestionId == questionId)
                .FirstOrDefault();
        }

        public async Task SetUserAnswersAsync(ApplicationUser user, List<SecurityAnswer> securityAnswers)
        {
            user.SecurityAnswers.Clear();
            securityAnswers.ForEach(s => user.SecurityAnswers.Add(s));
            await Context.SaveChangesAsync();
        }

        public async Task<ICollection<TasbRole>> GetTasbRolesByNames(string[] roles)
        {
            var result = new List<TasbRole>();
            foreach (var role in roles)
            {
                var tasbRole = await Context.TasbRoles
                    .Where(r => r.Name.Equals(role.Trim(), StringComparison.OrdinalIgnoreCase))
                    .FirstOrDefaultAsync();

                if (tasbRole == null)
                {
                    tasbRole = new TasbRole() { Name = role };
                }
                result.Add(tasbRole);
            }
            return result;
        }

        public bool HasPermission(ApplicationUser user, string permissionName)
        {
            foreach (var role in user.TasbRoles
                                    .AsQueryable()
                                    .Include(t => t.TasbRoles)
                                    .Include(t => t.Permissions)
                                    .Traverse(r => r.TasbRoles))
            {
                if (role.Permissions
                    .Where(p => p.Name.Equals(permissionName,StringComparison.OrdinalIgnoreCase))
                    .Any())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
