﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TASB.SpEd.DataAccess.Models
{
    public class Student
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LocationId { get; set; }

        public virtual Location Location { get; set; }

        public Student()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
