﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TASB.SpEd.Business;
using Microsoft.AspNet.Identity.Owin;
using TASB.SpEd.Web.Models;
using TASB.SpEd.DataAccess.Models;

namespace TASB.SpEd.Web.Controllers
{
    public class StudentController : Controller
    {
        public StudentRepository Students
        {
            get
            {
                return HttpContext.GetOwinContext().Get<StudentRepository>();
            }
        }

        public LocationRepository Locations
        {
            get
            {
                return HttpContext.GetOwinContext().Get<LocationRepository>();
            }
        }

        public ActionResult Index(string filter)
        {
            List<Student> students;
            if (string.IsNullOrEmpty(filter))
            {
                students = Students.GetAllStudents();
            }
            else
            {
                students = Students.GetStudentsByFirstName(filter);
            }

            var list = new List<StudentViewModel>();
            students.ForEach(s => list.Add(new StudentViewModel() { FirstName = s.FirstName, LastName = s.LastName, Location = s.Location.LocationName }));

            var model = new ListStudentsViewModel();
            model.Students = list;
            return View(model);
        }

        public ActionResult ByLocation(string filter)
        {
            List<Student> students;
            if (string.IsNullOrEmpty(filter))
            {
                students = Students.GetAllStudents();
            }
            else
            {
                students = Students.GetStudentsByLocationName(filter);
            }

            var list = new List<StudentViewModel>();
            students.ForEach(s => list.Add(new StudentViewModel() { FirstName = s.FirstName, LastName = s.LastName, Location = s.Location.LocationName }));

            var model = new ListStudentsViewModel();
            model.Students = list;
            return View(model);
        }
    }
}