﻿CREATE PROCEDURE [dbo].[GetStudentsByLocation]
    @location [nvarchar](128)
AS
BEGIN
    SELECT * FROM [dbo].[Students] s
	INNER JOIN Locations l
	ON s.LocationId = l.Id
    WHERE (l.LocationName = @location)
END