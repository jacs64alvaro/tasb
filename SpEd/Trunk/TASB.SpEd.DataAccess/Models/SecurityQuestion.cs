﻿using System;

namespace TASB.SpEd.DataAccess.Models
{
    public class SecurityQuestion
    {
        public string Id { get; set; }

        public string Text { get; set; }

        public SecurityQuestion()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
