﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TASB.SpEd.Util.Pipeline;
using TASB.SpEd.Util.Logging;

namespace TASB.SpEd.Util.Processors
{
    public class LogProcessor
    {
        public void Log(PipelineArgs args)
        {
            if (args.Data.ContainsKey("Message"))
            {
                LogManager.Info(args.Data["Message"].ToString());
            }
        }
    }
}
