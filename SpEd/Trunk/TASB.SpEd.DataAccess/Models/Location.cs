﻿using System;
using System.Collections.Generic;

namespace TASB.SpEd.DataAccess.Models
{
    public class Location
    {
        public string Id { get; set; }
        public string LocationName { get; set; }
        public string LocationType { get; set; }
        public string LocationId { get; set; }

        public virtual List<Location> Locations { get; set; }

        public Location()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
