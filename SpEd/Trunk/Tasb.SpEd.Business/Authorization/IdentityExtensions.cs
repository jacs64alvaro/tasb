﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Security.Principal;
using System.Web;

namespace TASB.SpEd.Business.Authorization
{
    public static class IdentityExtensions
    {
        public static bool HasPermission(this IIdentity identity, HttpContextBase httpContext, string permission)
        {
            var context = httpContext.GetOwinContext();
            return HasPermission(identity, permission, context);
        }

        public static bool HasPermission(this IIdentity identity, string permission)
        {
            var context = HttpContext.Current.GetOwinContext();
            return HasPermission(identity, permission, context);
        }

        private static bool HasPermission(IIdentity identity, string permission, IOwinContext context)
        {
            var manager = context.GetUserManager<ApplicationUserManager>();
            var repository = context.Get<ApplicationRepository>();
            var user = manager.FindById(identity.GetUserId());
            if (user != null)
            {
                return repository.HasPermission(user, permission);
            }

            return false;
        }
    }
}