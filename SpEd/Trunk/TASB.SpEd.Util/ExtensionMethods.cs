﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace TASB.SpEd.Util
{
    public static class ExtensionMethods
    {
        public static T Random<T>(this IEnumerable<T> input)
        {
            return EnumerableHelper<T>.Random(input);
        }

        public static IEnumerable<T> Traverse<T>(this IEnumerable<T> items, Func<T, IEnumerable<T>> childSelector)
        {
            var stack = new Stack<T>(items);
            while (stack.Any())
            {
                var next = stack.Pop();
                yield return next;
                foreach (var child in childSelector(next))
                    stack.Push(child);
            }
        }

        private static class EnumerableHelper<E>
        {
            private static Random r;

            static EnumerableHelper()
            {
                r = new Random();
            }

            public static T Random<T>(IEnumerable<T> input)
            {
                return input.ElementAt(r.Next(input.Count()));
            }
        }
    }
}
