﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASB.SpEd.Util.Logging
{
    public static class LogManager
    {
        private static readonly ILog Log = log4net.LogManager.GetLogger(typeof(LogManager));

        public static void Info(string message)
        {
            Log.Info(message);
        }

        public static void Error(Exception ex, string message = "Error Found:")
        {
            Log.Error(String.Format("{0}{1}{2}", message, Environment.NewLine, ex));
        }

        public static void Warning(string message)
        {
            Log.Warn(message);
        }



    }
}
