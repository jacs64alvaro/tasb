﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TASB.SpEd.Business;
using TASB.SpEd.Business.Authorization;
using TASB.SpEd.DataAccess;
using TASB.SpEd.DataAccess.Models;
using TASB.SpEd.Util;
using TASB.SpEd.Util.Pipeline;
using TASB.SpEd.Web.Models;

namespace TASB.SpEd.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
        {
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public ApplicationRepository Repository
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationRepository>();
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
                }
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, true);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult Lockout()
        {
            return View();
        }

        [HasPermission("CreateUser")]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HasPermission("CreateUser")]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    TasbRoles = await Repository.GetTasbRolesByNames(model.Roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                };

                var result = await UserManager.CreateAsync(user, Membership.GeneratePassword(30, 5));
                if (result.Succeeded)
                {
                    var args = new PipelineArgs();
                    args.Data["Message"] = string.Format("User {0} successfully registered by {1}", user.UserName, User.Identity.Name);
                    CorePipeline.Run("event:UserRegistered", args);

                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("ShowEmailTemp", "Account", new { callbackUrl = callbackUrl });
                }
                AddErrors(result);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ShowEmailTemp(string callbackUrl)
        {
            //TODO: remove this when email confirmation is ready
            ViewBag.CallbackUrl = callbackUrl;
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(userId);
                var passwordResetCode = await UserManager.GeneratePasswordResetTokenAsync(userId);

                return RedirectToAction("ResetPassword", "Account", new { Email = user.Email, Code = passwordResetCode });
            }
            return View("Error");
        }

        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string email, string code)
        {
            if (code == null)
            {
                return View("Error");
            }

            var model = new ResetPasswordViewModel();
            model.Email = email;
            model.Code = code;

            var user = await UserManager.FindByEmailAsync(email);
            if (user != null && user.SecurityAnswers.Count == 0)
            {
                model.Answers = Repository.Context.SecurityQuestions
                    .Select(q => new SecurityAnswerViewModel()
                    {
                        QuestionId = q.Id,
                        QuestionText = q.Text
                    }).ToList();
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            if (model.Password.ToLowerInvariant().Contains(user.FirstName.ToLowerInvariant())
                || model.Password.ToLowerInvariant().Contains(user.LastName.ToLowerInvariant()))
            {
                ModelState.AddModelError("", "Password cannot contain your first or last name");
                return View(model);
            }

            var validatePasswordResult = await UserManager.PasswordValidator.ValidateAsync(model.Password);
            if (!validatePasswordResult.Succeeded)
            {
                AddErrors(validatePasswordResult);
                return View(model);
            }

            if (model.Answers != null && model.Answers
                .Where(a => string.IsNullOrEmpty(a.Answer))
                .Any())
            {
                ModelState.AddModelError("", "Answers are required");
                return View(model);
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                if (model.Answers != null)
                {
                    var securityAnswers = model.Answers
                        .Select(a => new SecurityAnswer
                        {
                            QuestionId = a.QuestionId,
                            Answer = UserManager.PasswordHasher.HashPassword(a.Answer)
                        }).ToList();

                    await Repository.SetUserAnswersAsync(user, securityAnswers);
                }
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            var model = new ForgotPasswordViewModel();
            model.Answers = new List<OptionalSecurityAnswerViewModel>();
            var questions = Repository.Context.SecurityQuestions.ToList();

            var questionsToAnswer = 2;//TODO: get from config?
            for (var i = 0; i < questionsToAnswer; ++i)
            {
                var randomQuestion = questions.Random();
                questions.Remove(randomQuestion);
                model.Answers.Add(new OptionalSecurityAnswerViewModel()
                {
                    QuestionId = randomQuestion.Id,
                    QuestionText = randomQuestion.Text
                });
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null 
                    || !(await UserManager.IsEmailConfirmedAsync(user.Id))
                    || await UserManager.IsLockedOutAsync(user.Id))
                {
                    return View("Error");
                }
                
                if (model.Answers
                    .Where(a => !string.IsNullOrEmpty(a.Answer))
                    .Any())
                {
                    if (await CheckAnswers(user, model))
                    {
                        var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        return RedirectToAction("ResetPassword", "Account", new { email = user.Email, code = code });
                    }
                    else
                    {
                        await UserManager.AccessFailedAsync(user.Id);
                        return View("Error");
                    }
                }
                else
                {
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    //return View("ForgotPasswordConfirmation");
                    var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { email = user.Email, code = code }, protocol: Request.Url.Scheme);
                    return RedirectToAction("ShowEmailTemp", "Account", new { callbackUrl = callbackUrl });
                }
            }

            return View(model);
        }

        private async Task<bool> CheckAnswers(ApplicationUser user, ForgotPasswordViewModel model)
        {
            foreach (var answer in model.Answers)
            {
                if (string.IsNullOrEmpty(answer.Answer))
                {
                    return false;
                }
                var userAnswer = Repository.GetSecurityAnswerByUser(user, answer.QuestionId);

                if (userAnswer == null)
                {
                    return false;
                }

                var result = UserManager.PasswordHasher.VerifyHashedPassword(userAnswer.Answer, answer.Answer);
                if (result != PasswordVerificationResult.Success)
                {
                    return false;
                }
            }
            return true;
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion
    }
}