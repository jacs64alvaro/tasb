﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Web;
using System.Web.Hosting;
using TASB.SpEd.Business.Authorization;
using TASB.SpEd.DataAccess;
using TASB.SpEd.DataAccess.Models;

namespace TASB.SpEd.Business
{
    public class DatabaseInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var manager = ApplicationUserManager.CreateWithDbContext(new IdentityFactoryOptions<ApplicationUserManager>(), context);

            var questions = CreateQuestions(context);
            var roles = CreateRoles(context);
            var students = CreateStudents(context);

            var admin = new ApplicationUser()
            {
                UserName = "admin@test.com",
                Email = "admin@test.com",
                FirstName = "Admin",
                LastName = "Super",
                EmailConfirmed = true,
                SecurityAnswers = new List<SecurityAnswer>() {
                    new SecurityAnswer() {QuestionId = questions[0].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[1].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[2].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[3].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[4].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[5].Id, Answer=manager.PasswordHasher.HashPassword("answer")}
                },
                TasbRoles = new List<TasbRole>()
                {
                    roles[0]//admin
                }
            };

            manager.CreateAsync(admin, "Pa$$w0rd").Wait();

            var user = new ApplicationUser()
            {
                UserName = "user@test.com",
                Email = "user@test.com",
                EmailConfirmed = true,
                FirstName = "User",
                LastName = "Regular",
                SecurityAnswers = new List<SecurityAnswer>() {
                    new SecurityAnswer() {QuestionId = questions[0].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[1].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[2].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[3].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[4].Id, Answer=manager.PasswordHasher.HashPassword("answer")},
                    new SecurityAnswer() {QuestionId = questions[5].Id, Answer=manager.PasswordHasher.HashPassword("answer")}
                },
                TasbRoles = new List<TasbRole>()
                {
                    roles[1]//user
                }
            };

            manager.CreateAsync(user, "Pa$$w0rd").Wait();

            RunDatabaseScripts(context.Database);
        }

        private void RunDatabaseScripts(Database database)
        {
            foreach (var file in Directory.EnumerateFiles(Path.Combine(HostingEnvironment.ApplicationPhysicalPath,"bin","scripts"), "*.sql"))
            {
                var text = File.ReadAllText(file);
                database.ExecuteSqlCommand(text);
            }
        }

        private static List<TasbRole> CreateRoles(ApplicationDbContext context)
        {
            var deleteUser = new Permission() { Name = "DeleteUser" };
            var createUser = new Permission() { Name = "CreateUser" };
            var modifyUser = new Permission() { Name = "ModifyUser" };
            var editContent = new Permission() { Name = "EditContent" };

            var user = new TasbRole() 
            { 
                Name = "User",
                Permissions = new List<Permission>() { editContent }
            };
            var contentCreator = new TasbRole() 
            { 
                Name = "ContentCreator",
                TasbRoles = new List<TasbRole>() { user },
                Permissions = new List<Permission>() { createUser }
            };

            var contentManager = new TasbRole() 
            { 
                Name = "ContentManager",
                TasbRoles = new List<TasbRole>() { contentCreator },
                Permissions = new List<Permission>() { modifyUser }
            };

            var admin = new TasbRole() 
            {
                Name="Admin",
                TasbRoles = new List<TasbRole>() { contentManager },
                Permissions = new List<Permission>() { deleteUser }
            };
            
            var roles = new List<TasbRole>()
            {
                admin,
                user,
                contentManager,
                contentCreator,
            };
            roles.ForEach(r => context.TasbRoles.Add(r));
            context.SaveChanges();
            return roles;
        }

        private static List<SecurityQuestion> CreateQuestions(ApplicationDbContext context)
        {
            var questions = new List<SecurityQuestion>()
            {
                new SecurityQuestion() {Text="How old are you?"},
                new SecurityQuestion() {Text="What is your best friend's name?"},
                new SecurityQuestion() {Text="What is the name of your first pet?"},
                new SecurityQuestion() {Text="Favorite color?"},
                new SecurityQuestion() {Text="Favorite sport?"},
                new SecurityQuestion() {Text="Favorite sports team?"},
            };
            questions.ForEach(q => context.SecurityQuestions.Add(q));
            context.SaveChanges();
            return questions;
        }

        private static List<Student> CreateStudents(ApplicationDbContext context)
        {
            var l1 = new Location() {LocationName="Texas1",LocationType="Campus"};
            var l2 = new Location() { LocationName = "Texas2", LocationType = "District", Locations = new List<Location>() { l1 } };
            var l3 = new Location() { LocationName = "Texas3", LocationType = "Cooperative", Locations = new List<Location>(){ l2 } };

            var locations = new List<Location>()
            {
                l1,l2,l3
            };
            locations.ForEach(q => context.Locations.Add(q));

            var students = new List<Student>()
            {
                new Student() {FirstName="Diego",LastName="Jimenez",LocationId=l1.Id},
                new Student() {FirstName="Steven",LastName="Walker",LocationId=l2.Id},
                new Student() {FirstName="Raiam",LastName="Quesada",LocationId=l3.Id},
                new Student() {FirstName="Brian",LastName="Usrey",LocationId=l1.Id},
                new Student() {FirstName="Adam",LastName="Herrneckar",LocationId=l2.Id},
                new Student() {FirstName="Ric",LastName="Lara",LocationId=l3.Id}
            };
            students.ForEach(q => context.Students.Add(q));

            context.SaveChanges();
            return students;
        }
    }
}