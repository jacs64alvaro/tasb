﻿CREATE PROCEDURE [dbo].[GetStudentsByFirstName]
    @name [nvarchar](128)
AS
BEGIN
    SELECT * FROM [dbo].[Students]
    WHERE ([FirstName] = @name)
END